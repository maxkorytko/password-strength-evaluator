# Password Strength Evaluator

This project implements a password strength evalutor in Java. The password strength evaluator takes a password as an input and returns its strength as an output. It can be used in Android applications, for example.

The rules used to determine the strength of the password are outlined in the 'Password Strength Rules Chat.pdf' file.

The code is not designed to provide the ability to add custom rules. It should be easy to re-factor the code to do so, however.