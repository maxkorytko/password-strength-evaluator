package com.maxk;

import static com.maxk.PasswordStrengthRule.*;

import com.google.common.collect.Range;

import java.util.Arrays;
import java.util.List;

/**
 * Abstract factory for creating password strength rules for all supported password strength categories.
 *
 * Created by maxkorytko on 15-05-25.
 */
public abstract class PasswordStrengthRulesFactory {
    public static class NotLongEnough extends PasswordStrengthRulesFactory {
        @Override
        public List<PasswordStrengthRules> makeRules() {
            return Arrays.asList(
                    new PasswordStrengthRules(Range.atMost(5), new PasswordStrengthRule() {
                        @Override
                        public boolean apply(CharSequence password) {
                            return true;
                        }
                    }));
        }
    }

    public static class Weak extends PasswordStrengthRulesFactory {
        @Override
        public List<PasswordStrengthRules> makeRules() {
            return Arrays.asList(
                    new PasswordStrengthRules(Range.atLeast(6),
                            ALL_LOWER_CASE.or(ALL_UPPER_CASE).or(ALL_NUMBERS).or(ALL_SYMBOLS)),

                    new PasswordStrengthRules(Range.closed(6, 7), LOWER_CASE.and(NUMBER)),
                    new PasswordStrengthRules(Range.closed(6, 9), LOWER_CASE.and(UPPER_CASE))
            );
        }
    }

    public static class Good extends PasswordStrengthRulesFactory {
        @Override
        public List<PasswordStrengthRules> makeRules() {
            return Arrays.asList(
                    new PasswordStrengthRules(Range.closed(6, 9),
                            LOWER_CASE.and(NUMBER).and(SYMBOL).or(LOWER_CASE.and(UPPER_CASE).and(SYMBOL))),

                    new PasswordStrengthRules(Range.closed(6, 10), LOWER_CASE.and(NUMBER).and(UPPER_CASE)),
                    new PasswordStrengthRules(Range.closed(6, 11), UPPER_CASE.and(NUMBER)),
                    new PasswordStrengthRules(Range.closed(6, 12), UPPER_CASE.and(SYMBOL)),
                    new PasswordStrengthRules(Range.closed(6, 13), LOWER_CASE.and(SYMBOL).or(NUMBER.and(SYMBOL))),
                    new PasswordStrengthRules(Range.closed(8, 14), LOWER_CASE.and(NUMBER)),
                    new PasswordStrengthRules(Range.closed(10, 15), LOWER_CASE.and(UPPER_CASE))
            );
        }
    }

    public static class Strong extends PasswordStrengthRulesFactory {
        @Override
        public List<PasswordStrengthRules> makeRules() {
            return Arrays.asList(
                    new PasswordStrengthRules(Range.closed(6, 11), LOWER_CASE.and(NUMBER).and(UPPER_CASE).and(SYMBOL)),

                    new PasswordStrengthRules(Range.closed(8, 11),
                            LOWER_CASE.and(NUMBER).and(SYMBOL).or(LOWER_CASE.and(UPPER_CASE).and(SYMBOL))),

                    new PasswordStrengthRules(Range.closed(8, 13), LOWER_CASE.and(NUMBER).and(UPPER_CASE)),
                    new PasswordStrengthRules(Range.closed(12, 18), NUMBER.and(UPPER_CASE)),
                    new PasswordStrengthRules(Range.closed(13, 17), UPPER_CASE.and(SYMBOL)),
                    new PasswordStrengthRules(Range.closed(14, 18), NUMBER.and(SYMBOL)),
                    new PasswordStrengthRules(Range.closed(14, 19), LOWER_CASE.and(SYMBOL)),
                    new PasswordStrengthRules(Range.closed(15, 19), LOWER_CASE.and(NUMBER)),
                    new PasswordStrengthRules(Range.closed(16, 21), LOWER_CASE.and(UPPER_CASE))
            );
        }
    }

    public static class VeryStrong extends PasswordStrengthRulesFactory {
        @Override
        public List<PasswordStrengthRules> makeRules() {
            return Arrays.asList(
                    new PasswordStrengthRules(Range.atLeast(8), LOWER_CASE.and(NUMBER).and(UPPER_CASE).and(SYMBOL)),

                    new PasswordStrengthRules(Range.atLeast(12),
                            LOWER_CASE.and(NUMBER).and(SYMBOL).or(LOWER_CASE.and(UPPER_CASE).and(SYMBOL))),

                    new PasswordStrengthRules(Range.atLeast(14), LOWER_CASE.and(NUMBER).and(UPPER_CASE)),
                    new PasswordStrengthRules(Range.atLeast(18), NUMBER.and(UPPER_CASE).or(UPPER_CASE.and(SYMBOL))),
                    new PasswordStrengthRules(Range.atLeast(19), NUMBER.and(SYMBOL)),
                    new PasswordStrengthRules(Range.atLeast(20), LOWER_CASE.and(NUMBER).or(LOWER_CASE.and(SYMBOL))),
                    new PasswordStrengthRules(Range.atLeast(22), LOWER_CASE.and(UPPER_CASE))
            );
        }
    }

    /**
     * Creates and returns a list of rules for a specific password strength category.
     */
    public abstract List<PasswordStrengthRules> makeRules();
}
