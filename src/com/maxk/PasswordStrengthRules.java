package com.maxk;

import com.google.common.collect.Range;

/**
 * Encapsulates an association between a password length range and rules which determine the password strength.
 * Using this class you can express different rules for passwords that are 4 and 10 characters long, for example.
 *
 * Created by maxkorytko on 15-05-25.
 */
public final class PasswordStrengthRules {
    private final Range mPasswordLengthRange;
    private final PasswordStrengthRule mRule;

    public PasswordStrengthRules(Range lengthRange, PasswordStrengthRule rule) {
        if (lengthRange == null) {
            throw new IllegalArgumentException("lengthRange");
        }

        if (rule == null) {
            throw new IllegalArgumentException("rule");
        }

        mPasswordLengthRange = lengthRange;
        mRule = rule;
    }

    /**
     * Applies the rule to the given password.
     * Returns true if the password satisfies the rule and false otherwise.
     */
    public boolean apply(CharSequence password) {
        if (password == null) {
            throw new IllegalArgumentException("password");
        }

        return mPasswordLengthRange.contains(password.length()) ? mRule.apply(password) : false;
    }
}
