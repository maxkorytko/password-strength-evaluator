package com.maxk;

import java.util.List;

/**
 * Enum for the supported password strength categories.
 *
 * Created by maxkorytko on 15-05-25.
 */
public enum PasswordStrength {
    NOT_LONG_ENOUGH(new PasswordStrengthRulesFactory.NotLongEnough().makeRules()),
    WEAK(new PasswordStrengthRulesFactory.Weak().makeRules()),
    GOOD(new PasswordStrengthRulesFactory.Good().makeRules()),
    STRONG(new PasswordStrengthRulesFactory.Strong().makeRules()),
    VERY_STRONG(new PasswordStrengthRulesFactory.VeryStrong().makeRules());

    private final List<PasswordStrengthRules> mEvaluationRules;

    PasswordStrength(List<PasswordStrengthRules> rules) {
        if (rules == null) {
            throw new IllegalArgumentException("rules");
        }

        mEvaluationRules = rules;
    }

    /**
     * Determines if the given password satisfies any of the rules associated with a specific strength category.
     */
    public boolean appliesTo(CharSequence password) {
        for (PasswordStrengthRules rules : mEvaluationRules) {
            if (rules.apply(password)) {
                return true;
            }
        }

        return false;
    }
}
