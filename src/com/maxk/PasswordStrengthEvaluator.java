package com.maxk;

/**
 * Performs password strength evaluation.
 *
 * Created by maxkorytko on 15-05-25.
 */
public class PasswordStrengthEvaluator {
    /**
     * Evaluates the given password and returns its strength.
     */
    public static PasswordStrength evaluate(CharSequence password) {
        if (password == null) {
            throw new IllegalArgumentException("password");
        }

        PasswordStrength passwordStrength = PasswordStrength.WEAK;

        if (PasswordStrength.NOT_LONG_ENOUGH.appliesTo(password)) {
            passwordStrength = PasswordStrength.NOT_LONG_ENOUGH;
        } else if (PasswordStrength.VERY_STRONG.appliesTo(password)) {
            passwordStrength = PasswordStrength.VERY_STRONG;
        } else if (PasswordStrength.STRONG.appliesTo(password)) {
            passwordStrength = PasswordStrength.STRONG;
        } else if (PasswordStrength.GOOD.appliesTo(password)) {
            passwordStrength = PasswordStrength.GOOD;
        }

        return passwordStrength;
    }
}
