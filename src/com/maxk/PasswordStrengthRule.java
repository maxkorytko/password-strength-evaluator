package com.maxk;

import com.google.common.base.CharMatcher;

/**
 * Represents a single rule that is used to determine the strange of any given password.
 *
 * Created by maxkorytko on 15-05-25.
 */
public abstract class PasswordStrengthRule {
    private static final CharSequence VALID_SYMBOLS = "!~`@#$%^&*-+();:={}[],.<>?\\/\"'";

    /**
     * Password contains at least one lower case character.
     */
    public static final PasswordStrengthRule LOWER_CASE = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.JAVA_LOWER_CASE.matchesAnyOf(password);
        }
    };
    /**
     * Password contains at least one upper case character.
     */
    public static final PasswordStrengthRule UPPER_CASE = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.JAVA_UPPER_CASE.matchesAnyOf(password);
        }
    };
    /**
     * Password contains at least one number character.
     */
    public static final PasswordStrengthRule NUMBER = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.JAVA_DIGIT.matchesAnyOf(password);
        }
    };
    /**
     * Password contains at least one symbol character.
     */
    public static final PasswordStrengthRule SYMBOL = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.anyOf(VALID_SYMBOLS).matchesAnyOf(password);
        }
    };
    /**
     * Password contains all lower case characters.
     */
    public static final PasswordStrengthRule ALL_LOWER_CASE = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.JAVA_LOWER_CASE.matchesAllOf(password);
        }
    };
    /**
     * Password contains all upper case characters.
     */
    public static final PasswordStrengthRule ALL_UPPER_CASE = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.JAVA_UPPER_CASE.matchesAllOf(password);
        }
    };
    /**
     * Password contains all number characters.
     */
    public static final PasswordStrengthRule ALL_NUMBERS = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.JAVA_DIGIT.matchesAllOf(password);
        }
    };
    /**
     * Password contains all symbol characters.
     */
    public static final PasswordStrengthRule ALL_SYMBOLS = new PasswordStrengthRule() {
        @Override
        public boolean apply(CharSequence password) {
            return CharMatcher.anyOf(VALID_SYMBOLS).matchesAllOf(password);
        }
    };

    /**
     * Applies the rule to the given password.
     * Returns true if the password satisfies the rule and false otherwise.
     */
    public abstract boolean apply(CharSequence password);

    /**
     * Creates a new rule by composing this rule and another rule.
     * The composite rule applies only if either of the rules being composed applies.
     */
    public PasswordStrengthRule or(PasswordStrengthRule anotherRule) {
        return or(this, anotherRule);
    }

    private static PasswordStrengthRule or(PasswordStrengthRule left, PasswordStrengthRule right) {
        return new PasswordStrengthRule() {
            @Override
            public boolean apply(CharSequence password) {
                return left.apply(password) || right.apply(password);
            }
        };
    }

    /**
     * Creates a new rule by composing this rule and another rule.
     * The composite rule applies only if both of the rules being composed apply.
     */
    public PasswordStrengthRule and(PasswordStrengthRule anotherRule) {
        return and(this, anotherRule);
    }

    private static PasswordStrengthRule and(PasswordStrengthRule left, PasswordStrengthRule right) {
        return new PasswordStrengthRule() {
            @Override
            public boolean apply(CharSequence password) {
                return left.apply(password) && right.apply(password);
            }
        };
    }
}
