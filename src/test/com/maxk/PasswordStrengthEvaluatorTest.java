package test.com.maxk;

import junit.framework.TestCase;

import static com.maxk.PasswordStrength.*;
import static com.maxk.PasswordStrengthEvaluator.*;

/**
 * Created by maxkorytko on 15-05-24.
 */
public class PasswordStrengthEvaluatorTest extends TestCase {
    public void testPasswordNotLongEnough() {
        assertTrue(evaluate("") == NOT_LONG_ENOUGH);
        assertTrue(evaluate("h") == NOT_LONG_ENOUGH);
        assertTrue(evaluate("hi") == NOT_LONG_ENOUGH);
        assertTrue(evaluate("hello") == NOT_LONG_ENOUGH);
        assertTrue(evaluate("hello1") != NOT_LONG_ENOUGH);
    }

    public void testPasswordIsWeak() {
        assertTrue(evaluate("hellothere") == WEAK);
        assertTrue(evaluate("PASSWORD") == WEAK);
        assertTrue(evaluate("1234567") == WEAK);
        assertTrue(evaluate("$%!@()-") == WEAK);
        assertTrue(evaluate("pass123") == WEAK);
        assertTrue(evaluate("PASSword") == WEAK);
    }

    public void testPasswordIsGood() {
        assertTrue(evaluate("l3tm3innow") == GOOD);
        assertTrue(evaluate("1paSSwordyouME") != GOOD);
        assertTrue(evaluate("password4you2me") != GOOD);
    }

    public void testPasswordIsStrong() {
        assertTrue(evaluate("password4you2me") == STRONG);
        assertTrue(evaluate("Pasw0$d") == STRONG);
        assertTrue(evaluate("1paSSwordyouME") != STRONG);
    }

    public void testPasswordIsVeryStrong() {
        assertTrue(evaluate("1paSSwordyouME") == VERY_STRONG);
        assertTrue(evaluate("testingaverystrongpassword1") == VERY_STRONG);
        assertTrue(evaluate("TestgAVeryStrongPassword") == VERY_STRONG);
        assertTrue(evaluate("testingaverystrongpas$()\\") == VERY_STRONG);
        assertTrue(evaluate("ThisIsSomeTestPassword_0") == VERY_STRONG);
        assertTrue(evaluate("testingaverystrongpassword1") == VERY_STRONG);
        assertTrue(evaluate("Passw0$d") == VERY_STRONG);
    }
}